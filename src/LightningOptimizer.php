<?php

namespace Drupal\lightningcss;

use Drupal\Core\Asset\CssOptimizer;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\InfoParserInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;

/**
 * Optimizes a CSS asset.
 */
class LightningOptimizer extends CssOptimizer {
  /**
   * Binary path for lightningcss.
   *
   * @var string
   */
  private static $binary;

  /**
   * Browserlist cache.
   *
   * @var array<string>
   */
  private static $browserlist = [];

  /**
   * List of themes subpaths with lightningcss enabled.
   *
   * @var array<string>
   */
  private static $subpaths;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The info parser.
   *
   * @var \Drupal\Core\Extension\InfoParserInterface
   */
  protected $infoParser;

  /**
   * Cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cache;

  /**
   * Constructs a CssOptimizer.
   *
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module manager.
   * @param \Drupal\Core\Extension\InfoParserInterface $info_parser
   *   The info parser.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache.
   */
  public function __construct(
    FileUrlGeneratorInterface $file_url_generator,
    ThemeHandlerInterface $theme_handler,
    ModuleHandlerInterface $module_handler,
    InfoParserInterface $info_parser,
    CacheBackendInterface $cache
    ) {
    parent::__construct($file_url_generator);
    $this->themeHandler = $theme_handler;
    $this->moduleHandler = $module_handler;
    $this->infoParser = $info_parser;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function optimize(array $css_asset) {
    if ($css_asset['type'] != 'file') {
      throw new \Exception('Only file CSS assets can be optimized.');
    }
    if (!$css_asset['preprocess']) {
      throw new \Exception('Only file CSS assets with preprocessing enabled can be optimized.');
    }

    return $this->chooseStrategy($css_asset);
  }

  /**
   * Processes CSS file and adds base URLs to any relative resource paths.
   *
   * @param array $css_asset
   *   A CSS asset.
   *
   * @return string
   *   The asset's cleaned/optimized contents.
   */
  protected function chooseStrategy($css_asset) {
    self::$subpaths ??= $this->cache->get('extensions')?->data ?? $cache = [
      ...array_map(fn($theme) => $theme->subpath, array_filter($this->themeHandler->listInfo(), function ($item) {
        return isset($item->info['lightningcss']);
      })),
      ...array_map(fn($module) => $module->getPath(), array_filter($this->moduleHandler->getModuleList(), function ($item) {
        return isset($this->infoParser->parse(DRUPAL_ROOT . '/' . $item->getPathname())['lightningcss']);
      })),
    ];
    isset($cache) && $this->cache->set('extensions', $cache);

    $compile = array_filter(self::$subpaths, function ($item) use ($css_asset) {
      return str_starts_with($css_asset['data'], $item);
    });

    return count($compile)
      ? $this->compileFile($css_asset, array_values($compile)[0])
      : $this->processFile($css_asset);
  }

  /**
   * Processes CSS file and adds base URLs to any relative resource paths.
   *
   * @param array $css_asset
   *   A CSS asset. The array should contain the `data` key where the value
   *   should be the path to the CSS file relative to the Drupal root. This is
   *   an example of the `data` key's value,
   *   "core/assets/vendor/normalize-css/normalize.css".
   * @param strring $subpath
   *   Subpath of theme or module.
   *
   * @return string
   *   The asset's cleaned/optimized contents.
   */
  protected function compileFile($css_asset, $subpath) {
    $binary = $this->getBinary();
    $browserlist = $this->getBrowserlist($subpath);
    $file = DRUPAL_ROOT . "/" . $css_asset['data'];
    $contents = exec("{$binary} {$file} --minify --bundle --nesting --targets='{$browserlist}' 2>&1", $out, $err);

    if ($err) {
      throw new \Exception("Error compiling {$file}: {$err}\n{$out[0]}");
    }

    // Get the parent directory of this file, relative to the Drupal root.
    $css_base_path = substr($css_asset['data'], 0, strrpos($css_asset['data'], '/'));
    // Store base path.
    $this->rewriteFileURIBasePath = $css_base_path . '/';

    // Anchor all paths in the CSS with its base URL, ignoring external
    // and absolute paths and paths starting with '#'.
    return preg_replace_callback('/url\(\s*[\'"]?(?![a-z]+:|\/+|#|%23)([^\'")]+)[\'"]?\s*\)/i', [
      $this,
      'rewriteFileURI',
    ], $contents);
  }

  /**
   * Get Browserlist config.
   *
   * @param string $subpath
   *   Subpath of theme or module.
   *
   * @return string
   *   Browserlist config.
   */
  protected function getBrowserlist($subpath): string {
    if (in_array($subpath, array_keys(self::$browserlist))) {
      return self::$browserlist[$subpath];
    }

    $file = DRUPAL_ROOT . "/" . $subpath . "/.browserslistrc";
    if (file_exists($file) && $contents = @file_get_contents($file)) {
      return self::$browserlist[$subpath] = str_replace(["\n", "\r"], ',', trim($contents));
    }

    $file = DRUPAL_ROOT . "/" . $subpath . "/package.json";
    if (file_exists($file) && $contents = @file_get_contents($file)) {
      $data = json_decode($contents, TRUE);
      if (isset($data['browserslist'])) {
        return self::$browserlist[$subpath] = implode(", ", $data['browserslist']);
      }
    }

    return implode(', ', [
      'last 2 Chrome versions',
      'last 2 Edge versions',
      'last 2 ff versions',
      'last 2 Safari versions',
      'last 2 ios_saf versions',
      'last 2 and_ff versions',
      'last 2 and_chr versions',
    ]);
  }

  /**
   * Gets the path to the lightningcss binary.
   *
   * @return string
   *   Binary Path.
   */
  protected function getBinary() {
    if (!self::$binary) {
      $libc = $this->detectLibc();
      $os = $this->detectOperatingSystem();
      $arch = $this->detectArchitecture();

      $parts = [$os, $arch];
      if ($os === 'linux') {
        if ($libc === 'musl') {
          $parts[] = 'musl';
        }
        elseif ($arch === 'arm') {
          $parts[] = 'gnueabihf';
        }
        else {
          $parts[] = 'gnu';
        }
      }
      if ($os === 'win32') {
        $parts[] = 'msvc';
      }
      self::$binary =
        DRUPAL_ROOT .
        "/libraries/lightningcss-cli-" .
        implode('-', $parts) . "/lightningcss" .
        ($os === 'win32' ? '.exe' : '');
    }

    return self::$binary;
  }

  /**
   * Check which libc is being used.
   *
   * @return string
   *   Libc.
   */
  protected function detectLibc() {
    $libcVersion = shell_exec("ldd --version 2>&1");

    return match(TRUE) {
      strpos($libcVersion, 'musl') !== FALSE => "musl",
      strpos($libcVersion, 'GNU C Library') !== FALSE => "glibc",
      default => "unknown",
    };
  }

  /**
   * Get the architecture.
   *
   * @return string
   *   arch.
   */
  protected function detectArchitecture(): string {
    $arch = php_uname("m");
    return match(TRUE) {
      strpos($arch, 'x86_64') !== FALSE || strpos($arch, 'amd64') !== FALSE => "x64",
      strpos($arch, 'arm') !== FALSE => "ARM",
      default => "unknown",
    };
  }

  /**
   * Get the operating system.
   *
   * @return string
   *   OS.
   */
  protected function detectOperatingSystem() {
    $uname = php_uname('s');

    return match(TRUE) {
      strpos(strtolower($uname), 'windows') !== FALSE => "win32",
      strpos(strtolower($uname), 'linux') !== FALSE => "linux",
      strpos(strtolower($uname), 'darwin') !== FALSE => "darwin",
      default => "unknown",
    };
  }

}
