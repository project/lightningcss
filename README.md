# LightningCSS

This module allows to use [LightningCSS](https://lightningcss.dev/) to compile styles from themes and modules.
To use it simply put:
```yml
lightningcss: true
```
into your themes or modules *.info.yml

Browserlist configuration is respected (.browserlistrc, package.json). Enviroments are not supported.

The module uses the lightningcss cli with command:
```sh
lightningcss --minify --bundle --nesting --targets="last 2 Chrome versions, last 2 Edge versions, last 2 ff versions, last 2 Safari versions, last 2 ios_saf versions, last 2 and_ff versions, last 2 and_chr versions"
```
